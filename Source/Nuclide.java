package project2;

public class Nuclide {
	
	double halflife;
	double number;
	double absorptioncrs;
	double fissionyield;
	
	public Nuclide(){
	}
	
	public Nuclide(double t12,double crs,double yield){
		halflife=t12*3600;
		absorptioncrs=crs*10e-24;
		fissionyield=yield/100;
	}
	
	public void setT12(Double double1){
		halflife = double1;
	}
	
	public void setNumber(Double double1){
		number = double1;
	}
	
	public void setCrs(double double1){
		absorptioncrs = double1;
	}
	
	public void setYield(double double1){
		fissionyield = double1;
	}
	
	public double getYield(){
		return fissionyield;
	}
	
	public double getIV(){
		return number;
	}
	
	public double getCrs(){
		return absorptioncrs;
	}
	
	public Double getT12(){
		return halflife;
	}
	
	public double decayConstant(){
		return 0.6934718056/(halflife);
	}
}
