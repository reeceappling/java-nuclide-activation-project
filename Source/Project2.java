package project2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

public class Project2 {
	
	//Global Class Variables
	boolean tlock;
	ArrayList<tripledouble> concentrations;
	double fcrs[]; //array of fast and thermal macroscopic fission crosssections
	double flux []; //array of fast and thermal fluxes
	double dt; //Delta t, timestep change variable
	int num; //number of nuclides
	Nuclide[] n; //array of nuclides
	PowerInterval[] p; //array of power intervals
	double a; //reused placeholder variable
	double b; //reused placeholder variable
	double c; //reused placeholder variable
	int i; //reused placeholder variable
	int j;
	int k;
	double u;
	double r;
	double q;
	double[] s = new double[8];
	double[] maxvals = new double[2];
	String path; //path to input file
	File f;
	String temp;
	//End Global Class Variables
	
	public static void main(String[] args){
		new Project2();
	}
	
	public Project2(){
		setInitialVariables(); //sets any initial variable values
		getinput(); //gets all needed user input
		calculate(); //does calculations
		output(); //formats and posts output
		dt=1;
		calculate();
		s[0]=r;
		s[1]=q;
		s[4]=a;
		s[5]=b;
		while((s[0]-s[2])/s[0] >= 0.0001 || (s[0]-s[2])/s[2] <=-0.0001 || (s[1]-s[3])/s[1] >= 0.0001 || (s[1]-s[3])/s[1] <=-0.0001){
			dt=u;
			calculate();
			s[2]=r;
			s[3]=q;
			s[6]=a;
			s[7]=b;
			u=dt/2;
		}
		System.out.println("The best value of dt to use is " + u/1800);
		System.out.println("The maximums are:");
		System.out.println(s[0] + " at t=" + s[4] + " for nuclide 1");
		System.out.println(s[1] + " at t=" + s[5] + " for nuclide 2");
		
	}
	
	private void setInitialVariables() {
		flux = new double[2]; //instantiates flux array
		flux[0] = 0.00; //sets fast flux to 0 before reading user input
		flux[1] = 0.00; //sets thermal flux to 0 before reading user input
		fcrs = new double[2];
		fcrs[0]=0.00;
		fcrs[1]=0.00;
		num=0; // sets number of nuclides to 0 before reading user input
		dt = 0.00; // sets timestep to 0 before reading user input
		p = new PowerInterval[4]; //instantiates power interval array
		path = "./input.txt";
	}

	private void getinput() {	
		i=0;j=0;
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		//check for input file
		f = new File(path);
		if(f.exists() && !f.isDirectory()) {
			ArrayList<String> inputs = new ArrayList<String>();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(path));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
			    String line = br.readLine();
			    inputs.add(line);
			    while (line != null) {
			        line = br.readLine();
			        inputs.add(line);
			    }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
			    try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			j=0;a=0;b=0;c=0;i=0;
			while (j < inputs.size()-1){
				String[] parse = inputs.get(j).split(":");
				String linecase = parse[0];
				switch (linecase){
				case "fastflux": flux[0] = Double.valueOf(parse[1]);
				break;
				case "thermalflux": flux[1] = Double.valueOf(parse[1]);
				break;
				case "numberofnuclides": num = Integer.valueOf(parse[1]);
				n = new Nuclide[num];
				break;
				case "nuclide1t12": a = Double.valueOf(parse[1]);
				break;
				case "nuclide1abs": b = Double.valueOf(parse[1]);
				break;
				case "nuclide1yield": c = Double.valueOf(parse[1]);
				n[i] = new Nuclide(a,b,c); //creates new nuclide object with the user's input, stores in an array
				i=i+1;
				break;
				case "nuclide2t12": a = Double.valueOf(parse[1]);
				break;
				case "nuclide2abs": b = Double.valueOf(parse[1]);
				break;
				case "nuclide2yield": c = Double.valueOf(parse[1]);
				n[i] = new Nuclide(a,b,c); //creates new nuclide object with the user's input, stores in an array
				i=0;
				break;
				case "timestep": dt = Double.valueOf(parse[1])*3600;
				u=dt;
				break;
				case "powerint1": a = Double.valueOf(parse[1]);
				break;
				case "timeint1": b = Double.valueOf(parse[1]);
				p[i] = new PowerInterval(a,b*3600); // create new power interval object using the user's input, store it in an array
				i=i+1;
				break;
				case "powerint2": a = Double.valueOf(parse[1]);
				break;
				case "timeint2": b = Double.valueOf(parse[1]);
				p[i] = new PowerInterval(a,b*3600); // create new power interval object using the user's input, store it in an array
				i=i+1;
				break;
				case "powerint3": a = Double.valueOf(parse[1]);
				break;
				case "timeint3": b = Double.valueOf(parse[1]);
				p[i] = new PowerInterval(a,b*3600); // create new power interval object using the user's input, store it in an array
				i=i+1;
				break;
				case "powerint4": a = Double.valueOf(parse[1]);
				break;
				case "timeint4": b = Double.valueOf(parse[1]);
				p[i] = new PowerInterval(a,b*3600); // create new power interval object using the user's input, store it in an array
				i=0;
				break;
				case "fastfissioncrs": fcrs[0] = Double.valueOf(parse[1]);
				break;
				case "thermalfissioncrs": fcrs[1] = Double.valueOf(parse[1]);
				break;
				}
				j=j+1;
			}
			//check if values are acceptable
			if(valuesAcceptable()) {
				j=1;
			}
			else{
				System.out.println("The values provided in your input file were unacceptable");
				System.out.println("You can either close the program and edit your file, or input values manually");
				j=0;
			}
		}
		else{ //if input file does not exist, copy file from inside jar to outside jar
			System.out.println("This appears to be your first time running this program,");
			try {
				ExportResource("./input.txt");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("The file input.txt has been generated outside the jar file");
			System.out.println("at " + temp);
			System.out.println("You may use it to input values which will run alongside the program in the future");
			System.out.println("For this instance you must input the values manually.");
			j=0;
		}
		if(j==0){
		while (flux[0]<=0.00){
				System.out.println("Please enter the fast flux at full power (n/(s*cm^2)):");
				flux[0] = Double.valueOf(reader.next()); // Sets the fast flux to the user's input.
			}
		while (flux[1]<=0.00){
				System.out.println("Please enter the thermal flux at full power (n/(s*cm^2)):");
				flux[1] = Double.valueOf(reader.next()); // Sets the thermal flux to the user's input.
			}
		while (fcrs[0]<=0.00){
			System.out.println("Please enter the fast fission crossection at full power (/cm):");
			flux[0] = Double.valueOf(reader.next()); // Sets the fast flux to the user's input.
		}
		while (fcrs[1]<=0.00){
			System.out.println("Please enter the thermal fission crossection at full power (/cm):");
			flux[1] = Double.valueOf(reader.next()); // Sets the thermal flux to the user's input.
		}
		while(num<=0){
			System.out.println("Please Enter the number of nuclides:");
			num = Integer.valueOf(reader.next()); // Scans the next token of the input as an int.
		}
		n = new Nuclide[num];
		i = 1;
		while (i-1<num){
			a=-1.00;
			while (a<0){
				System.out.println("What is the half life of nuclide " + i + " in hours?");
				System.out.println("If this nuclide is stable, please input 0.");
				a = Double.valueOf(reader.next()); // Sets the half life to the user's input.
				//if a nuclide is stable, set its half life to 0.
			}
			b=-1.00;
			while (b<=0){
				System.out.println("What is the thermal absorption cross-section of nuclide " + i + " in barns?");
				b = Double.valueOf(reader.next()); // Sets the thermal absorption crossection to the user's input.
			}
			c=-1.00;
			while (c<0 || c>100){
				System.out.println("What is the fission product yield of nuclide " + i + " in %?");
				c = Double.valueOf(reader.next()); // Sets the fission product yield to the user's input.
			}
			n[i-1] = new Nuclide(a,b,c); //creates new nuclide object with the user's input, stores in an array
			i=i+1;
		}
		while (dt<=0.00){
			System.out.println("What time step would you like to use? (hours)");
			dt = Double.valueOf(reader.next())*3600; // Sets the timestep to the user's input.
		}
		i=1;
		while (i-1<4){
			a=-1.00;
			while (a<0.00 || a>100.00){
				System.out.println("What is the reactor power (in %) of interval " + i + "?");
				a = Double.valueOf(reader.next()); // Sets the timestep to the user's input.
			}
			b=-1.00;
			while (b<=0.00){
				System.out.println("What is the time in hours of interval" + i + "?");
				b = Double.valueOf(reader.next()); // Sets the timestep to the user's input.
			}
			p[i-1] = new PowerInterval(a,b); // create new power interval object using the user's input, store it in an array
			i=i+1;
		}
		}
		reader.close(); //close user input scanner
		flux[0]=flux[0];
		flux[1]=flux[1];
		u=dt;
	}

	private boolean valuesAcceptable() {
		i=0;k=0;
		if (fcrs[0]<=0.00 || fcrs[1]<=0.00 || flux[0]<=0.00 || flux[1]<=0.00 || num<=0 || dt<= 0.00){return false;}
		while(i<p.length){
			if(p[i].power<00 || p[i].power>100 || p[i].t<=0.00){System.out.println("power unacceptable");return false;}
			i=i+1;
		}
		i=0; while(i<n.length){
			if(n[i].halflife<0 || n[i].absorptioncrs<0 || n[i].fissionyield<0 || n[i].fissionyield>100){return false;}
			i=i+1;
		}
		return true;
	}
	
	public void ExportResource(String resourceName) throws Exception {
        InputStream stream = null;
        OutputStream resStreamOut = null;
        String jarFolder;
            stream = Project2.class.getResourceAsStream(resourceName);//note that each / is a directory down in the "jar tree" been the jar the root of the tree
            if(stream == null) {
                throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");}
            int readBytes;
            byte[] buffer = new byte[4096];
            jarFolder = new File(Project2.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile().getPath().replace('\\', '/');
            temp = jarFolder;
            resStreamOut = new FileOutputStream(jarFolder + resourceName);
            while ((readBytes = stream.read(buffer)) > 0) {
                resStreamOut.write(buffer, 0, readBytes);
            }
            stream.close();
            resStreamOut.close();
    }
	
	@SuppressWarnings("unused")
	private void DisplayValues(){
		int l=0;
		System.out.println("Fast Flux: " + flux[0]);
		System.out.println("Thermal Flux: " + flux[1]);
		System.out.println("Nuclides: " + n.length);
		while(l<n.length){
			System.out.println("Half life of nuclide " + l + " : " + n[l].halflife);
			System.out.println("Absorption crs of nuclide " + l + " : " + n[l].absorptioncrs);
			System.out.println("Fission yield of nuclide " + l + " : " + n[l].fissionyield);
			l=l+1;
		}
		l=0;
		System.out.println("Timestep: " + dt);
		while(l<4){
			System.out.println("Timestep " + l + " power: " + p[l].power);
			System.out.println("Timestep " + l + " time: " + p[l].t);
			l=l+1;
		}
	}
	
	@SuppressWarnings("unused")
	private void check(int m){
		System.out.println("Code checks out to checkpoint " + m);
	}
	
	private void calculate() {
		concentrations = null;
		double maxt = 0.00;
		double thistime = 0.00;
		tripledouble cprev = new tripledouble();
		tripledouble cnow = new tripledouble();
		double[] pflux = new double[3]; //fluxes when accounting for power, the last is the average flux
		concentrations = new ArrayList<tripledouble>();
		cprev.set(0,0.00);cprev.set(1,0.00);cprev.set(2,0.00);
		concentrations.add(new tripledouble(cprev.get(0),cprev.get(1),cprev.get(2)));
		i=0;j=0;k=0;
		maxt= p[k].t;
		while(k<p.length){
			pflux[0]=flux[0]*(p[k].power/100);
			pflux[1]=flux[1]*(p[k].power/100);
			pflux[2]=pflux[0]+pflux[1];
			a=pflux[2];
			double avgFluxTimesFissionCrs = ((fcrs[0]*pflux[0])+(fcrs[1]*pflux[1]));
			//calculate one timestep
			if(k<4){maxt= maxt+(p[k].t);}
			while(thistime<maxt){
				//calculate next concentrations, new cnow
				double lambdaprime;
				cnow.set(0,cprev.get(0)+dt);
				cnow.set(1,((1-(n[0].decayConstant()*dt))*cprev.get(1))+(n[0].fissionyield*avgFluxTimesFissionCrs*dt));
				if(n[1].halflife !=0){lambdaprime=((n[1].decayConstant())+(n[1].absorptioncrs*pflux[1]));}
				else{lambdaprime=(n[1].absorptioncrs*pflux[1]);}
				double section1=(1-(lambdaprime*dt))*cprev.get(2);
				double section2=(n[1].fissionyield*avgFluxTimesFissionCrs*dt);
				double section3=(n[0].decayConstant()*dt*cprev.get(1));
				cnow.set(2,section1+section2+section3);
				//add next concentrations
				concentrations.add(new tripledouble(cnow.get(0)/3500,cnow.get(1),cnow.get(2)));
				//set last concentrations to the newly found ones
				cprev.set(2,new Double(cnow.get(2)));
				cprev.set(1,new Double(cnow.get(1)));
				cprev.set(0,new Double(cnow.get(0)));
				//set new timestamp
				thistime = new Double(cnow.get(0)+dt);
			}
			k=k+1;
		}
		i=0;r=0;q=0;
		while(i<concentrations.size()){
			if(concentrations.get(i).get(1)>r){r=concentrations.get(i).get(1);a=concentrations.get(i).get(0);}
			if(concentrations.get(i).get(2)>q){q=concentrations.get(i).get(2);b=concentrations.get(i).get(0);}
			i=i+1;
		}
	}
	
	private void output() {	
		i=0;
		try {
			FileWriter w = new FileWriter("./output.csv");
			w.write(getdata(concentrations.get(0)));
			int k = 1;
			while(k<concentrations.size()){
				w.write("\r\n" + getdata(concentrations.get(k)));
				k=k+1;
			}
			w.flush();
			w.close();
			
		} catch (UnsupportedEncodingException e) {
			System.out.println("Error: Output file path could not be decoded");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error: Filewriter could not be opened");
		}
		System.out.println("If a program error has not been thrown, the data can be found in output.csv");
		System.out.println("Which has been saved in the same folder as this program's .jar file");
		System.out.println(r + " at t=" + a + " for nuclide 1");
		System.out.println(q + " at t=" + b + " for nuclide 2");
		System.out.println("Any further running of the program will be to find the optimal timestep");
	}
	
	private String getdata(tripledouble tripledouble){
		StringBuilder sb = new StringBuilder();
		sb.append(Double.toString(tripledouble.get(0)));
		int counter = 1;
		while(counter<3){
			sb.append("," + Double.toString(tripledouble.get(counter)));
			counter = counter+1;
		}
		String out = sb.toString();
		return out;
	}
}
