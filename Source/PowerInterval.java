package project2;

public class PowerInterval {
	
	double power; //variable for reactor power (%)
	double t; //time interval

	public PowerInterval(double pct, double time){
		power=pct;
		t=time;
	}
	
	public double getPower(){
		return power;
	}
	
	public void setpower(double pwr){
		power=pwr;
	}
	
	public double getTime(){
		return t;
	}
	
	public void setTime(double time){
		t=time;
	}
}
