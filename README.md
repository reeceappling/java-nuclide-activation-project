# Java Nuclide Activation Project

An old project in which the task was to find concentrations of nuclides interrelated via nuclear reactions in a specific thermal neutron flux, with other specific properties for each nuclide.

Please extract the entire contents of the zip folder this may be contained in before running

This program can be run on any operating system that has java installed.
I have made a "shortcut" for running the program on windows computers, the batch file (appling_project2program.bat).
If your operating system cannot open batch files, please run Project2.jar through the terminal window.
Do not use javaw to run the program, as the program requires user input through the terminal window
and javaw does not run programs through the terminal window.

once the program has run with acceptable user input, the data will be output into a csv file
located within the same directory as the program, labelled output.csv

source code for the jar file (the uncompiled java files) can be found within the source folder
the source for the .bat file can be found by selecting "edit" rather than run on the .bat file
due to the fact that .bat files do not need to be compiled before they are distribution ready

if there is no input.txt provided with this copy of the program, dont worry,
the program will generate one on any run where it does not find one in the same file
and prompt for manual user input. Although it is recommended to use input.txt to input your initial data.

do not edit anything but the numbers in input.txt, this can break the program. If this somehow happens, delete input.txt
and the program will re-generate it on the next run.

Here is what each part of input.txt means, with example values for each, 
everything / and beyond should be considered a comment and should not be found in the actual input.txt
values should not have a space between the : and the number

fastflux:2.861e14		/This is the fast flux in /(cm^2*s)

thermalflux:0.653e14		/This is the thermal flux in /(cm^2*s)

fastfissioncrs:0.00225		/This is the fast fission cross section in /(cm)

thermalfissioncrs:0.03772	/This is the thermal fission cross section in /(cm)

numberofnuclides:2		/This is the number of nuclides

nuclide1t12:6.7			/This is the half life of nuclide 1 in hours

nuclide1abs:0			/This is the microscopic absorption cross section of nuclide 1 in barns

nuclide1yield:0.0639		/This is the fission yield of nuclide 1 in %

nuclide2t12:9.2			/This is the half life of nuclide 2 in hours

nuclide2abs:2.65e6		/This is the microscopic absorption cross section of nuclide 2 in barns

nuclide2yield:0.00237		/This is the fission yield of nuclide 2 in %

timestep:0.01			/This is the timestep that will be used in hours

powerint1:20			/This is the percent of power for power interval 1 in %

timeint1:48			/This is the time that power interval 1 will run in hours

powerint2:100			/This is the percent of power for power interval 2 in %

timeint2:72			/This is the time that power interval 2 will run in hours

powerint3:0			/This is the percent of power for power interval 3 in %

timeint3:96			/This is the time that power interval 3 will run in hours

powerint4:100			/This is the percent of power for power interval 4 in %

timeint4:96			/This is the time that power interval 4 will run in hours
